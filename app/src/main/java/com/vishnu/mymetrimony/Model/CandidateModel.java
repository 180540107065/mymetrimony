package com.vishnu.mymetrimony.Model;

import android.content.pm.ServiceInfo;

import java.io.Serializable;
import java.lang.reflect.Constructor;

public class CandidateModel implements Serializable {

    int UserId;
    int Gender;
    int IsFavorite;
    int LanguageID;
    int CityID;
    String Name;
    String FatherName;
    String SurName;
    String Dob;
    String PhoneNumber;
    String Hobbies;
    String Email;


    public CandidateModel(int userId, int gender, int isFavorite, int languageID, int cityID, String name, String fatherName, String surName, String dob, String phoneNumber, String hobbies, String email) {
        UserId = userId;
        Gender = gender;
        IsFavorite = isFavorite;
        LanguageID = languageID;
        CityID = cityID;
        Name = name;
        FatherName = fatherName;
        SurName = surName;
        Dob = dob;
        PhoneNumber = phoneNumber;
        Hobbies = hobbies;
        Email = email;
    }

    public CandidateModel() {
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public int getCityID() {
        return CityID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    @Override
    public String toString() {
        return "CandidateModel{" +
                "UserId=" + UserId +
                ", Gender=" + Gender +
                ", IsFavorite=" + IsFavorite +
                ", LanguageID=" + LanguageID +
                ", CityID=" + CityID +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Dob='" + Dob + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }
}
