package com.vishnu.mymetrimony.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.vishnu.mymetrimony.Model.CandidateModel;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public  static  final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String GENDER = "Geder";
    public static final String DOB = "Dob";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String LANGUAGE_ID = "LanguageID";
    public static final String CITY_ID = "CityID";
    public static final String HOBBIES = "Hobbies";
    public static final String IS_FAVORITE = "IsFavorite";
    public static final String EMAIL = "Email";


    public TblUser(Context context) {
        super(context);
    }

    //List Candidate
    public ArrayList<CandidateModel> getAllCandidateList(){

        //Array List Empty Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sqlite Object
        SQLiteDatabase db = getReadableDatabase();

        //Sql Statement
        String query = "select * from "+TABLE_NAME;

        //Cursor Object
        Cursor cursor = db.rawQuery(query,null);

        //Reset Cursor Position
        cursor.moveToFirst();

        //One By One Object Insert Into List
        while(cursor.moveToNext()){

            //Candidate Model Object For One Line Data Set
            CandidateModel candidateModel = new CandidateModel();

            candidateModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            candidateModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            candidateModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            candidateModel.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
            candidateModel.setCityID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            candidateModel.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
            candidateModel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
            candidateModel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
            candidateModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVORITE)));
            candidateModel.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            candidateModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            candidateModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));

            //Object Add Into List
            candidateList.add(candidateModel);

        }

        //All Database Object Close
        db.close();
        cursor.close();

        return candidateList;


    }

    //Add Candidate Record
    public long insertCandidateRecord(CandidateModel candidateModel){
        //Array List Empty Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sqlite Object
        SQLiteDatabase db = getWritableDatabase();

        //Insert Data Into Content Values
        ContentValues cv = returnContentValuesFromCandidateModel(candidateModel);

        //Insert Into Db
        return db.insert(TABLE_NAME,null,cv);
    }

    public long updateCandidateRecord(CandidateModel candidateModel)
    {
        //Array List Empty Of Candidate Model
        ArrayList<CandidateModel> candidateList = new ArrayList<>();

        //Sqlite Object
        SQLiteDatabase db = getWritableDatabase();

        //Insert Data Into Content Values
        ContentValues cv = returnContentValuesFromCandidateModel(candidateModel);

        return db.update(TABLE_NAME,cv," "+USER_ID+" = ?",new String[]{String.valueOf(candidateModel.getUserId())});
    }

    public ContentValues returnContentValuesFromCandidateModel(CandidateModel candidateModel){

        ContentValues cv = new ContentValues();

        cv.put(NAME,candidateModel.getName());
        cv.put(FATHER_NAME,candidateModel.getFatherName());
        cv.put(SUR_NAME,candidateModel.getSurName());
        cv.put(GENDER,candidateModel.getGender());
        cv.put(DOB,candidateModel.getDob());
        cv.put(PHONE_NUMBER,candidateModel.getPhoneNumber());
        cv.put(LANGUAGE_ID,candidateModel.getLanguageID());
        cv.put(CITY_ID,candidateModel.getCityID());
        cv.put(HOBBIES,candidateModel.getHobbies());
        cv.put(IS_FAVORITE,candidateModel.getIsFavorite());
        cv.put(EMAIL,candidateModel.getEmail());

        return cv;
    }

}
