package com.vishnu.mymetrimony.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.vishnu.mymetrimony.Model.LanguageModel;

import java.util.ArrayList;

public class TblMstCity extends MyDatabase {

    public static final String TABLE_NAME = "TblMstCity";
    public  static  final String CITY_ID = "CityID";
    public static final String NAME = "Name";

    public TblMstCity(Context context) { super(context); }


    public ArrayList<LanguageModel> getLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (cursor.moveToNext()){
            LanguageModel languageModel = new LanguageModel();

            languageModel.setLanguageID(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            languageModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));

            list.add(languageModel);

        }
        cursor.close();
        db.close();
        return list;
    }
}

