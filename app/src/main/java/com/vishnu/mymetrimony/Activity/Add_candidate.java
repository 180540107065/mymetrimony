package com.vishnu.mymetrimony.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vishnu.mymetrimony.Model.CandidateModel;
import com.vishnu.mymetrimony.R;
import com.vishnu.mymetrimony.database.TblUser;

import java.util.Calendar;

public class Add_candidate extends AppCompatActivity{

    EditText add_candidate_etFirstname,add_candidate_etFathername,add_candidate_etsurname,add_candidate_etDob,add_candidate_etemailAdrees,add_candidate_etPhoneNumber;

    RadioButton add_candidate_rbmale,add_candidate_rbfemale;

    Spinner add_candidate_spBirthplace,add_candidate_splanguage;

    Button add_candidate_btnsubmit;

    //candidate model object
    CandidateModel candidateModel;

    //table user object

    TblUser tblUser;

    // check inserted succesfull
    long insertedCheck;

    //Temp varibale for get Data from user

    String name, fathername, surname,DOB,emial,phone;
    int spCityposition,spLanguageposition;
    boolean rbmale;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_candidate);
        getSupportActionBar().setTitle("Register");

        init();
        process();
        listners();



    }

    //Initialize All Object
    private void init() {
        add_candidate_etFirstname = findViewById(R.id.add_candidate_etFirstname) ;
        add_candidate_etFathername = findViewById(R.id.add_candidate_etFathername);
        add_candidate_etsurname = findViewById(R.id.add_candidate_etsurname);
        add_candidate_etDob = findViewById(R.id.add_candidate_etDob);
        add_candidate_etemailAdrees = findViewById(R.id.add_candidate_etemailAdrees);
        add_candidate_etPhoneNumber = findViewById(R.id.add_candidate_etPhoneNumber);

        add_candidate_rbmale = findViewById(R.id.add_candidate_rbmale);
        add_candidate_rbfemale = findViewById(R.id.add_candidate_rbfemale);

        add_candidate_spBirthplace = findViewById(R.id.add_candidate_spBirthplace);
        add_candidate_splanguage = findViewById(R.id.add_candidate_splanguage);


        add_candidate_btnsubmit = findViewById(R.id.add_candidate_btnsubmit);

        candidateModel = new CandidateModel();

        tblUser = new TblUser(this);



    }

    private void process() {

        // defult Focus on first name
        add_candidate_etFirstname.requestFocus();
    }

    private void listners() {

        add_candidate_btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (IsValidData()){

                    candidateModel.setName(name);
                    candidateModel.setFatherName(fathername);
                    candidateModel.setSurName(surname);
                    candidateModel.setEmail(emial);
                    candidateModel.setPhoneNumber(phone);
                    candidateModel.setCityID(1);
                    candidateModel.setLanguageID(1);
                    candidateModel.setDob(DOB);
                    candidateModel.setIsFavorite(0);
                    candidateModel.setGender(1);


                    //model pass into database

                    insertedCheck = tblUser.insertCandidateRecord(candidateModel);

                    if (insertedCheck>0){
                        Toast.makeText(Add_candidate.this, "Insert Succesfull", Toast.LENGTH_SHORT).show();
                        
                    }else{
                        Toast.makeText(Add_candidate.this, "Somethig went wrong", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }


    public boolean IsValidData(){

        boolean valid = true;

        getDataFromFields();

    // name is not less than 3
        if(name.length()<3){

            add_candidate_etFirstname.setError("Please Enter Some Details");
            add_candidate_etFirstname.requestFocus();
            valid = false;

        }

        if(TextUtils.isEmpty(name)){

            add_candidate_etFirstname.setError("Please Mobile Number Is 10 Digit");
            add_candidate_etFirstname.requestFocus();
            valid = false;

        }

        // father name is not less than 3
        if(fathername.length()<3){

            add_candidate_etFathername.setError("Please Enter Some Details");
            add_candidate_etFathername.requestFocus();
            valid = false;

        }
        if(TextUtils.isEmpty(fathername)){

            add_candidate_etFathername.setError("Please Enter Some Details");
            add_candidate_etFathername.requestFocus();
            valid = false;

        }

        // Sur name is not less than 3
        if(surname.length()<3){

            add_candidate_etFathername.setError("Please Enter Some Details");
            add_candidate_etFathername.requestFocus();
            valid = false;

        }
        if(TextUtils.isEmpty(surname)){

            add_candidate_etsurname.setError("Please Enter Some Details");
            add_candidate_etsurname.requestFocus();
            valid = false;

        }
        if(TextUtils.isEmpty(emial)){

            add_candidate_etemailAdrees.setError("Please Enter Some Details");
            add_candidate_etemailAdrees.requestFocus();
            valid = false;

        }
        if(TextUtils.isEmpty(phone)){

            add_candidate_etPhoneNumber.setError("Please Enter Some Details");
            add_candidate_etPhoneNumber.requestFocus();
            valid = false;

        }
    // mobile number excat 10 character
        if(phone.length()!=10){

            add_candidate_etPhoneNumber.setError("Please Enter Some Details");
            add_candidate_etPhoneNumber.requestFocus();
            valid = false;

        }
    return valid;

    }

    void getDataFromFields(){

        //Edit text temp variable
        name = dataSpacseRomve(add_candidate_etFirstname.getText().toString());
        fathername = dataSpacseRomve(add_candidate_etFathername.getText().toString());
        surname = dataSpacseRomve(add_candidate_etsurname.getText().toString());
        phone = dataSpacseRomve(add_candidate_etPhoneNumber.getText().toString());
        emial = dataSpacseRomve(add_candidate_etemailAdrees.getText().toString());
        DOB = dataSpacseRomve(add_candidate_etDob.getText().toString());

        //Radio button temp variable

        rbmale = add_candidate_rbmale.isChecked();
    }

    String dataSpacseRomve(String s){

        s = s.replace("'","");
        s = s.trim();
        return s;
    }

}
