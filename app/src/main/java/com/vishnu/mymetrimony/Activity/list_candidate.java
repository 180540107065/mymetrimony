package com.vishnu.mymetrimony.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vishnu.mymetrimony.R;

public class list_candidate extends AppCompatActivity {

    private Intent i1 = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_candidate);
        getSupportActionBar().setTitle("List of Candidate");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.MyFab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // button 1 was clicked!
                i1.setClass(getApplicationContext(),Add_candidate.class);
                startActivity(i1);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id=item.getItemId();

        i1.setClass(getApplicationContext(),favourite_candidate.class);
        startActivity(i1);
        return super.onOptionsItemSelected(item);
    }
}