package com.vishnu.mymetrimony.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.vishnu.mymetrimony.R;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    public CardView card1 , card2 , card3 , card4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getSupportActionBar().hide();

        card1 = (CardView) findViewById(R.id.ADD);
        card2 = (CardView) findViewById(R.id.search);
        card3 = (CardView) findViewById(R.id.list);
        card4 = (CardView) findViewById(R.id.favourite);

        card1.setOnClickListener(this);
        card2.setOnClickListener(this);
        card3.setOnClickListener(this);
        card4.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent i;

        switch (view.getId()){

            case R.id.ADD :
                i = new Intent(this,Add_candidate.class);
                startActivity(i);
                break;

            case R.id.search :
                i = new Intent(this,search_candidate.class);
                startActivity(i);
                break;

            case R.id.list :
                i = new Intent(this,list_candidate.class);
                startActivity(i);
                break;

            case R.id.favourite :
                i = new Intent(this,favourite_candidate.class);
                startActivity(i);
                break;



        }
    }
}